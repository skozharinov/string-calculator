# String Calculator
Calculates mathematical expressions with the power of Dijkstra's shunting yard
algorithm and unique math library designed specially for this project!

### Compilation 
```bash
qmake Calculator.pro
make
```

## Usage
```bash
./Calculator
```

### Supported operations
* Arithmetic operations
   * Addition (+)
   * Subtraction (-)
   * Multiplication (*)
   * Division (/)
   * Exponentiation (^)
* Functions
   * Sine (sin)
   * Cosine (cos)
   * Exponential (exp)
   * Absolute value (abs)
   * Square root (sqrt)
   * Cubic root (cbrt)
   * Natural logarithm (ln)
   * Exponentiation (pow)
   * Logarithm (log)
   * Minimum of 2 arguments (min)
   * Maximum of 2 arguments (max)
* 2 kinds of brackets (round and square)
* Variables
