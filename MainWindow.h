#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QMap>

class QPushButton;
class QLineEdit;
class QHboxLayout;
class QVBoxLayout;

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(QWidget * = nullptr);

public slots:
    void calculate();

private:
    QPushButton *_calculateButton;
    QLineEdit *_inputEdit;
    QLineEdit *_postfixEdit;
    QLineEdit *_resultEdit;
    QVBoxLayout *_variables;

    QMap<QString, QLineEdit *> _variablesLineEdits;

    QStringList getVariablesNames() const;
    void showErrorMessage(const QString &message);
    void updateVariablesRows(const QStringList &variablesNames);
};

#endif // MAINWINDOW_HPP
