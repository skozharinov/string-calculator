#include "MainWindow.h"

#include "Number.h"
#include "Expression.h"
#include "Tokenizer.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    setWindowIcon(QIcon("://icon.svg"));

    auto *mainWidget = new QWidget();

    _inputEdit = new QLineEdit();
    _inputEdit->setMinimumWidth(400);
    _inputEdit->setPlaceholderText("Your expression");
    connect(_inputEdit, &QLineEdit::returnPressed, this, &MainWindow::calculate);

    _calculateButton = new QPushButton("Calculate");
    connect(_calculateButton, &QAbstractButton::clicked, this,
            &MainWindow::calculate);

    _postfixEdit = new QLineEdit();
    _postfixEdit->setPlaceholderText("Reverse Polish Notation");
    _postfixEdit->setReadOnly(true);

    _resultEdit = new QLineEdit();
    _resultEdit->setPlaceholderText("Calculation result");
    _resultEdit->setReadOnly(true);

    _variables = new QVBoxLayout();

    auto *layout = new QVBoxLayout();

    auto *inputRowLayout = new QHBoxLayout();

    inputRowLayout->addWidget(_inputEdit);
    inputRowLayout->addWidget(_calculateButton);

    layout->addLayout(inputRowLayout);
    layout->addWidget(_postfixEdit);
    layout->addWidget(_resultEdit);
    layout->addLayout(_variables);
    layout->addStretch();

    mainWidget->setLayout(layout);
    setCentralWidget(mainWidget);
}

void MainWindow::calculate() {
    auto expressionString = _inputEdit->text().toStdString();

    double result;

    Tokenizer tokenizer(expressionString);
    Expression expression({});
    QStringList variablesNames;

    try {
        auto tokens = tokenizer.getAllTokens();
        expression = Expression(tokens);
        variablesNames = getVariablesNames();
    }
    catch (const std::invalid_argument &e) {
        showErrorMessage(e.what());
        return;
    }

    if (variablesNames.empty()) {
        updateVariablesRows(variablesNames);
    }

    for (const auto &variableName : variablesNames) {
        if (_variablesLineEdits.contains(variableName)) {
            Number num;
            try {
                num = Number(Number::parseNumber(_variablesLineEdits[variableName]->text().toStdString()));
            } catch (const std::invalid_argument &e) {
                showErrorMessage(variableName + ": " + e.what());
                return;
            }

            auto var = variableName.toStdString();
            expression.setVariable(var, num);
        }
    }

    try{
        Number number = expression.evaluate();
        result = static_cast<double>(number);
    }
    catch (const std::invalid_argument &e) {
        showErrorMessage(e.what());
        return;
    }
    catch(const std::logic_error &e) {
        updateVariablesRows(variablesNames);
        return;
    }

    auto resultString = QString::number(result);
    _resultEdit->setText(resultString);

    auto postfixTokens = expression.getPostfix();
    QString postfix;
    while (!postfixTokens.isEmpty()) {
        auto token = postfixTokens.front();
        postfixTokens.pop();
        postfix += QString::fromStdString(token) + " ";
    }
    postfix = postfix.trimmed();
    _postfixEdit->setText(postfix);
}

void MainWindow::showErrorMessage(const QString &message) {
    _resultEdit->setText(message);
}

QStringList MainWindow::getVariablesNames() const {
    QStringList result;

    auto expressionString = _inputEdit->text().toStdString();
    Tokenizer tokenizer(expressionString);

    auto tokens = tokenizer.getAllTokens();
    Expression expression = Expression(tokens);
    auto variables = expression.getVariablesNames();
    for (std::size_t i = 0; i < variables.size(); i++) {
        result.push_back(QString::fromStdString(variables[i]));
    }

    return result;
}

void MainWindow::updateVariablesRows(const QStringList &variablesNames) {
    QLayoutItem *wItem;
    while ((wItem = _variables->takeAt(0)) != nullptr) {
        delete wItem->widget();
        delete wItem;
    }

    _variablesLineEdits.clear();

    for (const auto &variableName : variablesNames) {
        auto *variableLabel = new QLabel(variableName + " = ");
        auto *variableLineEdit = new QLineEdit();

        auto *variableRowLayout = new QHBoxLayout();
        variableRowLayout->addWidget(variableLabel);
        variableRowLayout->addWidget(variableLineEdit);

        auto *rowWidget = new QWidget();

        rowWidget->setLayout(variableRowLayout);
        _variables->addWidget(rowWidget);
        _variablesLineEdits[variableName] = variableLineEdit;
    }
}
